#include "include/parser.hpp"
#include "../common/include/command.hpp"
#include <vector>
std::string string = 
	{			
			"10. CLOCKWISE 810 °\n\
			20. PAUSE 500 ms\n\
			30. COUNTERCLOCKWISE 180°\n\
			40. PAUSE 500 ms\n\
			50. CLOCKWISE 270°\n\
			60. PAUSE 500 ms\n\
			70. COUNTERCLOCKWISE 765°\n\
			80. PAUSE 500 ms\n\
			90. COUNTERCLOCKWISE 135°\n\
			100. STOP\n\
			",
		};

int main()
{

	// for(auto)
	std::vector<command::command> foo;
	std::cout << string;
	parse_string(string, foo);



	// parse_command("CLOKWISE 180");

	return 0;
}