#ifndef __PARSER_HPP__
#define __PARSER_HPP__

#include <string>
#include "../../common/include/command.hpp"
#include <string>
#include <iostream>
#include <string.h>
#include <algorithm>
#include <vector>

bool parse_string(std::string commandString, std::vector<struct command::command>& commandsVector);
static bool parse_command(std::string command, struct command::command& commandStructObj);
static bool handle_rotation(std::string& command, struct command::command& commandStructObj);
static bool handle_pause(std::string& command, struct command::command& commandStructObj);
static void stop(struct command::command& commandStructObj);



#endif //__PARSER_H__
