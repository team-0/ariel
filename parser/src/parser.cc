#include "../include/parser.hpp"


constexpr const char* rotate_command = "CLOCKWISE";
constexpr const char* rotate_left_command = "COUNTERCLOCKWISE";
constexpr const char* pause_command = "PAUSE";
constexpr const char* stop_command = "STOP";

bool parse_string(std::string commandString, std::vector<struct command::command>& commandsVector)
{
	struct command::command commandStructObj;
	size_t possition = 0;
	size_t old_possition = 0;

	possition = commandString.find('\n', old_possition);

	while(possition != std::string::npos)
	{
		if(parse_command(commandString.substr(old_possition, possition), commandStructObj))
		{
			try
			{
				commandsVector.push_back(commandStructObj);
			}
			catch(...)
			{
				//TODO handle this!!!
				throw;
			}
		}
		else
		{
            return false;
		}

		old_possition = possition + 1;

		possition = commandString.find('\n', old_possition);
	}
}

static bool parse_command(std::string command, struct command::command& commandStructObj)
{
	command.erase(std::remove_if(command.begin(), command.end(), isspace), command.end());

	size_t possition = command.find(rotate_command);

	if(possition != std::string::npos)
	{
		return handle_rotation(command, commandStructObj);
	}

	possition = command.find(pause_command);

	if(possition != std::string::npos)
	{
		handle_pause(command, commandStructObj);
		//pause
		return true;
	}

	possition = command.find(stop_command);

	if(possition != std::string::npos)
	{
		stop(commandStructObj);
		//stop
		return true;
	}


	return false;
}

static bool handle_rotation(std::string& command, struct command::command& commandStructObj)
{
	size_t possition = command.find(rotate_left_command);

	uint32_t angle;
	
	commandStructObj.type = command::rotate;

	if(possition != std::string::npos)
	{
		possition += strlen(rotate_left_command);
		angle = std::stoi(command.substr(possition, 
												command.find_first_not_of("0123456789", possition)));

		commandStructObj.rotate_direction = command::left;
		commandStructObj.time_or_angle = angle;

		std::cout << "rotate left " << angle << std::endl;

		return true;
	}
	possition = command.find(rotate_command);

	possition += strlen(rotate_command);

	angle = std::stoi(command.substr(possition, 
											command.find_first_not_of("0123456789", possition)));

	commandStructObj.rotate_direction = command::right;
	commandStructObj.time_or_angle = angle;

	std::cout << "rotate right " << angle << std::endl;

	//rotate_right
	return true;

}
static bool handle_pause(std::string& command, struct command::command& commandStructObj)
{
	size_t possition = command.find(pause_command);

	possition += strlen(pause_command);

	size_t time = std::stoi(command.substr(possition, 
												command.find_first_not_of("0123456789", command.find(possition))));

	commandStructObj.type = command::pause;
	commandStructObj.time_or_angle = time;

	std::cout << "pause " << time << std::endl;

	return true;
}

static void stop(struct command::command& commandStructObj)
{
	commandStructObj.type = command::stop;

	std::cout << "stop " << std::endl;
}
