#include "spdlog/spdlog.h"
#include "spdlog/fmt/fmt.h"
#include "motorDriver.hpp"

#include <stdio.h>
#include <thread>
#include <chrono>

int main(int argc, char** argv )
{
    spdlog::set_level(spdlog::level::info);
    spdlog::info("Opencv test");

    using namespace ariel;

    Config cfg;

    cfg.mqttHost("localhost");
    cfg.mqttPort(1883);
    MotorDriver md(cfg);

    //md.subscribe("test", 2);
    //md.publish("test", "asdf test message");
    //md.freq(12);
    //md._freq("1337");
    //md.move({MotorDirection::CLOCKWISE, 20});
    //md.move({MotorDirection::ANTICLOCKWISE, 50});
    //md.move({MotorDirection::STILL, 20});

    while(1) {
      md.move({MotorDirection::CLOCKWISE, 10});
      std::this_thread::sleep_for(std::chrono::seconds(1));

      md.move({MotorDirection::ANTICLOCKWISE, 15});
      std::this_thread::sleep_for(std::chrono::seconds(1));

      md.move({MotorDirection::STILL, 0});
      std::this_thread::sleep_for(std::chrono::seconds(4));
    }
}
