#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/core/persistence.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/fmt/fmt.h>

#include <ipc/imageProcessor.hpp>

#include <chrono>
#include <signal.h>

volatile bool running = true;

void sig_int(int)
{
    spdlog::info("SIGINT received");
    running = false;
}

int main(int argc, char**argv)
{
    spdlog::set_level(spdlog::level::debug);
    spdlog::info("Opencv test");
    
    if ( argc < 3 )
    {
        spdlog::error("Usage: ");
        spdlog::error(" {0} <url> <config file>", argv[0]);
        return -1;
    }

    if (argc == 4)
    {
        std::string arg(argv[2]);
        if (arg == "udp")
        {
            setenv("OPENCV_FFMPEG_CAPTURE_OPTIONS", "rtsp_transport;udp", 1);
        }
    }

    try
    {
        ariel::ipc::State state;
        spdlog::info("Loading config file {}", argv[2]);
        cv::FileStorage fs(argv[2], cv::FileStorage::READ);
        state.Load(fs);

        ariel::ipc::ImageProcessor processor(state);
        processor.setMotorMoveInfo({ ariel::MotorDirection::CLOCKWISE, 0 });
        cv::VideoCapture cap(argv[1], cv::CAP_ANY);
        cap.set(CV_CAP_PROP_BUFFERSIZE, 1);

        if (! cap.isOpened())
        {
            spdlog::error("Cannot open capture");
            return -1;
        }

        signal(SIGINT, sig_int);

        cv::Mat frame;
        while(running)
        {
            auto start = std::chrono::high_resolution_clock::now();
            if (! cap.grab())
            {
                cap = cv::VideoCapture(argv[1], cv::CAP_ANY);
                cap.set(CV_CAP_PROP_BUFFERSIZE, 1);
                spdlog::warn("Capture failed");
                continue;
            }
            auto finish = std::chrono::high_resolution_clock::now();
            auto duration = finish - start;
            if ( duration / std::chrono::microseconds(1) < 1000.0/120.0 )
                continue;
            
            cap.retrieve(frame);

            processor.processFrame(frame);
            cv::waitKey(1000);
        }
    }
    catch(const std::exception& e)
    {
        spdlog::error("Caught exception {}", e.what());
    }
}
