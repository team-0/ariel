#pragma once
#include "config.hpp"
#include "motorMoveInfo.hpp"

#include <cstdint>

struct mosquitto;

namespace ariel {

  class MotorDriver
  {
    static constexpr std::string_view MOVE_TOPIC_STR = "move";
    static constexpr std::string_view FREQ_TOPIC_STR = "freq";

    static constexpr std::string_view LEFT_MOVE_STR = "left";
    static constexpr std::string_view RIGHT_MOVE_STR = "right";
    static constexpr std::string_view STOP_MOVE_STR = "stop";

    struct mosquitto *m_mosq_instance;

  public:
    MotorDriver(const Config &cfg);

    MotorDriver(const MotorDriver &other) = delete;
    MotorDriver(MotorDriver &&other) = delete;

    MotorDriver &
    operator=(const MotorDriver &other) = delete;

    MotorDriver &
    operator=(MotorDriver &&other) = delete;

    ~MotorDriver();

    void
    subscribe(const char *sub, int qos);

    void
    publish(const char *topic, std::string_view msg);

    void
    _freq(std::string_view freq_str) noexcept;

    void
    freq(std::uint_fast8_t freq_int);

    void
    move(MotorMoveInfo moveinfo) noexcept;

  private:
    static void
    connectCallback(struct mosquitto *, void *_this, int rc);

    static void
    disconnectCallback(struct mosquitto *, void *_this, int rc);

    static void
    messageCallback(struct mosquitto *,
                    void *_this,
                    const struct mosquitto_message *);

    static void
    printPayload(void *payload, int length) noexcept;
  };

} // namespace ariel
