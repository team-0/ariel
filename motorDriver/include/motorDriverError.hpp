#pragma once
#include <stdexcept>

namespace ariel::MotorDriverError {

  class ConnectionError : public std::runtime_error
  {
  public:
    int mosquitto_error_code;

    ConnectionError(const char *what, int error_code)
      : std::runtime_error(what)
      , mosquitto_error_code(error_code)
    {}
  };

  class SubscribeError : public std::runtime_error
  {
  public:
    int mosquitto_error_code;

    SubscribeError(const char *what, int error_code)
      : std::runtime_error(what)
      , mosquitto_error_code(error_code)
    {}
  };

} // namespace ariel::MotorDriver
