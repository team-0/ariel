#include "mosquitto.h"
#include "motorDriver.hpp"
#include "motorDriverError.hpp"
#include "spdlog/fmt/fmt.h"
#include "spdlog/spdlog.h"

#include <cstdio>

namespace ariel {

  MotorDriver::MotorDriver(const Config &cfg)
  {
    namespace MDE = MotorDriverError;
    mosquitto_lib_init();

    this->m_mosq_instance =
      mosquitto_new(NULL, /* Generate id */
                    true, /* must be true of first arg is NULL*/
                    this /* Callback argument */);

    int keepalive = 120;
    int connect_status = mosquitto_connect(this->m_mosq_instance,
                                           cfg.mqttHost().c_str(),
                                           cfg.mqttPort(),
                                           keepalive);

    if (connect_status != MOSQ_ERR_SUCCESS) {
      spdlog::error("Cant connect to broker at {}:{}.",
                    cfg.mqttHost(),
                    cfg.mqttPort());
      throw MDE::ConnectionError("Connection error", connect_status);
    }

    mosquitto_connect_callback_set(this->m_mosq_instance,
                                   &MotorDriver::connectCallback);

    mosquitto_message_callback_set(this->m_mosq_instance,
                                   &MotorDriver::messageCallback);
  }

  MotorDriver::~MotorDriver()
  {
    mosquitto_disconnect(this->m_mosq_instance);
    mosquitto_destroy(this->m_mosq_instance);
  }

  void
  MotorDriver::subscribe(const char *sub, int qos)
  {
    namespace MDE = MotorDriverError;

    int status =
      mosquitto_subscribe(this->m_mosq_instance, NULL, sub, qos);

    if (status != MOSQ_ERR_SUCCESS) {
      spdlog::error("Subscription failed on topic {}", sub);
      throw MDE::SubscribeError("Sub failed on topic: {}", status);
    }
  }

  void
  MotorDriver::publish(const char *topic, std::string_view msg)
  {
    int ret = mosquitto_publish(this->m_mosq_instance,
                                NULL,
                                topic,
                                msg.size(),
                                msg.begin(),
                                0,
                                false);

    if (ret != MOSQ_ERR_SUCCESS) {
      spdlog::error("Published failed! with code {}", ret);
      spdlog::info("Reconnecting . . .");

      int ret = mosquitto_reconnect(this->m_mosq_instance);

      if (ret == MOSQ_ERR_SUCCESS) {

        spdlog::info("Reconnected.");

      } else {

        spdlog::error("Reconnect failed!");
        throw MotorDriverError::ConnectionError(
          "Unexpectedly connection closed and reconnect failed.", ret);

      }

    }
  }

  void
  MotorDriver::_freq(std::string_view freq_str) noexcept
  {
    this->publish(MotorDriver::FREQ_TOPIC_STR.begin(), freq_str.begin());
  }

  void
  MotorDriver::freq(std::uint_fast8_t freq_int)
  {
    this->publish(MotorDriver::FREQ_TOPIC_STR.begin(),
                  fmt::format("{}", freq_int));
  }

  void
  MotorDriver::move(MotorMoveInfo moveinfo) noexcept
  {
    std::string_view moveCmd;

    if (moveinfo.speed == 0)
      moveinfo.direction = MotorDirection::STILL;

    switch (moveinfo.direction) {
      case MotorDirection::CLOCKWISE:
        spdlog::info("Clockwise, {}", moveinfo.speed);
        this->freq(moveinfo.speed);
        moveCmd = MotorDriver::RIGHT_MOVE_STR;
        break;
      case MotorDirection::ANTICLOCKWISE:
        spdlog::info("Anticlockwise, {}", moveinfo.speed);
        this->freq(moveinfo.speed);
        moveCmd = MotorDriver::LEFT_MOVE_STR;
        break;
      case MotorDirection::STILL:
        spdlog::info("Still, {}", moveinfo.speed);
        moveCmd = MotorDriver::STOP_MOVE_STR;
        break;
      default:
        spdlog::error("Passed unknown move direction!");
    }

    this->publish(MotorDriver::MOVE_TOPIC_STR.begin(), moveCmd);
  }

  void
  MotorDriver::connectCallback(struct mosquitto *mosq_instance,
                               void *_this,
                               int rc)
  {
    spdlog::info("Connect callback");
  }

  void
  MotorDriver::disconnectCallback(struct mosquitto *, void *_this, int rc)
  {
    spdlog::info("Disconnect callback");
  }

  void
  MotorDriver::messageCallback(struct mosquitto *,
                               void *_this,
                               const struct mosquitto_message *msg)
  {
    spdlog::info("Message callback");
    printPayload(msg->payload, msg->payloadlen);
  }

  void
  MotorDriver::printPayload(void *payload, int length) noexcept
  {
    printf("Payload: %.*s\n", length, static_cast<char *>(payload));
  }

} // namespace ariel
