#pragma once

#include "state.hpp"
#include "pipeline.hpp"
#include <motorMoveInfo.hpp>
#include <opencv2/opencv.hpp>
#include <memory>

namespace ariel::ipc
{
    class ImageProcessor
    {
        State m_state;
        std::unique_ptr<Pipeline> m_pipeline;
    public:
        ImageProcessor();
        ImageProcessor(const State& oldState);

        const State& state() const { return m_state; }
        State& state() { return m_state; }

        bool isCalibrated() const { return m_state.mode > Mode::POSE_SETUP; }

        void processFrame(cv::Mat frame);
        void setMotorMoveInfo(const MotorMoveInfo& moveInfo);
    };
}
