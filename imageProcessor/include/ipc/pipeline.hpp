#pragma once

#include <motorMoveInfo.hpp>

#include <opencv2/opencv.hpp>
#include <vector>

namespace ariel::ipc
{
    class ImageProcessor;

    class Pipeline
    {
    protected:
        ImageProcessor& m_processor;
    public:
        Pipeline(ImageProcessor& processor)
            : m_processor(processor)
        {}

        virtual const cv::Mat getDebugFrame() const { return cv::Mat(); }
        virtual void processFrame(cv::Mat frame) = 0;
        virtual bool isFinished() const = 0;
    };

    class MotorCalibrationPipeline : public Pipeline
    {
        struct Circle
        {
            cv::Point2f center;
            float radius;
        };

        std::vector<Circle> m_circles;
        size_t m_totalFramesCount;
        size_t m_framesCount;
        cv::Mat m_grayImg;
    public:
        MotorCalibrationPipeline(ImageProcessor& processor, size_t framesCount);

        const cv::Mat getDebugFrame() const override { return m_grayImg; }
        void processFrame(cv::Mat frame) override;
        bool isFinished() const override;
    private:
        void processCollectedData(cv::Mat lastFrame);
    };

    class MotorStillPipeline : public Pipeline
    {
        cv::Mat m_grayImg;
        std::vector<uint32_t> m_buffer;
    public:
        MotorStillPipeline(ImageProcessor& processor)
            : Pipeline(processor)
        {}

        const cv::Mat getDebugFrame() const override { return m_grayImg; }
        void processFrame(cv::Mat frame) override;
        bool isFinished() const override { return false; };
    };

    class MotorMovingPipeline : public Pipeline
    {
        MotorMoveInfo m_moveInfo;
        cv::Mat m_grayImg;
        std::vector<uint32_t> m_buffer;
    public:
        MotorMovingPipeline(ImageProcessor& processor, const MotorMoveInfo& moveInfo)
            : Pipeline(processor)
            , m_moveInfo(moveInfo)
        {}

        const cv::Mat getDebugFrame() const override { return m_grayImg; }
        void processFrame(cv::Mat frame) override;
        bool isFinished() const override { return false; };
    };
}
