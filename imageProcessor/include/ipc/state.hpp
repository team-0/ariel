#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/core/persistence.hpp>

namespace ariel::ipc
{
    enum class Mode
    {
        CALIBRATION,
        POSE_SETUP,
        DEMO
    };

    struct State
    {
        static constexpr const char* KEY_TRANSFORM = "transform";
        static constexpr const char* KEY_CENTER = "center";
        static constexpr const char* KEY_OUTER_RADIUS = "outer_radius";
        static constexpr const char* KEY_INNER_RADIUS = "inner_radius";

        Mode mode;
        cv::Mat transform;
        cv::Point2d center;
        cv::Point2d scale = {1.0,1.0};
        cv::Point3d rotation = {0.0,0.0,0.0};
        double outerRadius;
        double innerRadius;
        double currentAngle;

        cv::Mat calcTransform(cv::Size size);
        cv::Size calcSizeAfterTransform(cv::Size size);

        cv::Rect2d roi(cv::Point2d center)
        {
            cv::Rect2d roi;
            roi.x = center.x - outerRadius;
            roi.y = center.y - outerRadius;
            roi.width = outerRadius * 2;
            roi.height = outerRadius * 2;
            if (roi.x < 0)
            {
                roi.width += roi.x;
                roi.x = 0;
            }

            if (roi.y < 0)
            {
                roi.height += roi.y;
                roi.y = 0;
            }
            return roi;
        }

        cv::Rect2d roi()
        {
            return roi(center);
        }

        void Persist(cv::FileStorage& storage) const
        {
            storage << KEY_TRANSFORM << transform;
            storage << KEY_CENTER << center;
            storage << KEY_OUTER_RADIUS << outerRadius;
            storage << KEY_INNER_RADIUS << innerRadius;
        }

        void Load(cv::FileStorage& storage)
        {
            storage[KEY_TRANSFORM] >> transform;
            storage[KEY_CENTER] >> center;
            storage[KEY_OUTER_RADIUS] >> outerRadius;
            storage[KEY_INNER_RADIUS] >> innerRadius;
        }
    };
}