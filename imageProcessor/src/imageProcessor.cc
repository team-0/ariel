#include "ipc/imageProcessor.hpp"

#include <opencv2/highgui.hpp>
#include <spdlog/spdlog.h>

namespace
{
    constexpr bool DEBUG_EN = true;
    static double rad2Deg(double rad){return rad*(180/M_PI);}//Convert radians to degrees
    static double deg2Rad(double deg){return deg*(M_PI/180);}//Convert degrees to radians

    void warpMatrix(cv::Size   sz,
                double theta,
                double phi,
                double gamma,
                double scale,
                double fovy,
                cv::Mat&   M,
                std::vector<cv::Point2f>* corners){

        using namespace cv;
        double st=sin(deg2Rad(theta));
        double ct=cos(deg2Rad(theta));
        double sp=sin(deg2Rad(phi));
        double cp=cos(deg2Rad(phi));
        double sg=sin(deg2Rad(gamma));
        double cg=cos(deg2Rad(gamma));

        double halfFovy=fovy*0.5;
        double d=hypot(sz.width,sz.height);
        double sideLength=scale*d/cos(deg2Rad(halfFovy));
        double h=d/(2.0*sin(deg2Rad(halfFovy)));
        double n=h-(d/2.0);
        double f=h+(d/2.0);

        Mat F=Mat(4,4,CV_64FC1);//Allocate 4x4 transformation matrix F
        Mat Rtheta=Mat::eye(4,4,CV_64FC1);//Allocate 4x4 rotation matrix around Z-axis by theta degrees
        Mat Rphi=Mat::eye(4,4,CV_64FC1);//Allocate 4x4 rotation matrix around X-axis by phi degrees
        Mat Rgamma=Mat::eye(4,4,CV_64FC1);//Allocate 4x4 rotation matrix around Y-axis by gamma degrees

        Mat T=Mat::eye(4,4,CV_64FC1);//Allocate 4x4 translation matrix along Z-axis by -h units
        Mat P=Mat::zeros(4,4,CV_64FC1);//Allocate 4x4 projection matrix

        //Rtheta
        Rtheta.at<double>(0,0)=Rtheta.at<double>(1,1)=ct;
        Rtheta.at<double>(0,1)=-st;Rtheta.at<double>(1,0)=st;
        //Rphi
        Rphi.at<double>(1,1)=Rphi.at<double>(2,2)=cp;
        Rphi.at<double>(1,2)=-sp;Rphi.at<double>(2,1)=sp;
        //Rgamma
        Rgamma.at<double>(0,0)=Rgamma.at<double>(2,2)=cg;
        Rgamma.at<double>(0,2)=-sg;Rgamma.at<double>(2,0)=sg;

        //T
        T.at<double>(2,3)=-h;
        //P
        P.at<double>(0,0)=P.at<double>(1,1)=1.0/tan(deg2Rad(halfFovy));
        P.at<double>(2,2)=-(f+n)/(f-n);
        P.at<double>(2,3)=-(2.0*f*n)/(f-n);
        P.at<double>(3,2)=-1.0;
        //Compose transformations
        F=P*T*Rphi*Rtheta*Rgamma;//Matrix-multiply to produce master matrix

        //Transform 4x4 points
        double ptsIn [4*3];
        double ptsOut[4*3];
        double halfW=sz.width/2, halfH=sz.height/2;

        ptsIn[0]=-halfW;ptsIn[ 1]= halfH;
        ptsIn[3]= halfW;ptsIn[ 4]= halfH;
        ptsIn[6]= halfW;ptsIn[ 7]=-halfH;
        ptsIn[9]=-halfW;ptsIn[10]=-halfH;
        ptsIn[2]=ptsIn[5]=ptsIn[8]=ptsIn[11]=0;//Set Z component to zero for all 4 components

        Mat ptsInMat(1,4,CV_64FC3,ptsIn);
        Mat ptsOutMat(1,4,CV_64FC3,ptsOut);

        perspectiveTransform(ptsInMat,ptsOutMat,F);//Transform points

        //Get 3x3 transform and warp image
        Point2f ptsInPt2f[4];
        Point2f ptsOutPt2f[4];

        for(int i=0;i<4;i++){
            Point2f ptIn (ptsIn [i*3+0], ptsIn [i*3+1]);
            Point2f ptOut(ptsOut[i*3+0], ptsOut[i*3+1]);
            ptsInPt2f[i]  = ptIn+Point2f(halfW,halfH);
            ptsOutPt2f[i] = (ptOut+Point2f(1,1))*(sideLength*0.5);
        }

        M=getPerspectiveTransform(ptsInPt2f,ptsOutPt2f);

        //Load corners vector
        if(corners){
            corners->clear();
            corners->push_back(ptsOutPt2f[0]);//Push Top Left corner
            corners->push_back(ptsOutPt2f[1]);//Push Top Right corner
            corners->push_back(ptsOutPt2f[2]);//Push Bottom Right corner
            corners->push_back(ptsOutPt2f[3]);//Push Bottom Left corner
        }
    }

    void warpImage(const cv::Mat &src,
               double    theta,
               double    phi,
               double    gamma,
               double    scale,
               double    fovy,
               cv::Mat&      dst,
               cv::Mat&      M,
               std::vector<cv::Point2f> &corners
    ){
        using namespace cv;
        double halfFovy=fovy*0.5;
        double d=hypot(src.cols,src.rows);
        double sideLength=scale*d/cos(deg2Rad(halfFovy));

        warpMatrix(src.size(),theta,phi,gamma, scale,fovy,M,&corners);//Compute warp matrix
        warpPerspective(src,dst,M,Size(sideLength,sideLength));//Do actual image warp
    }
}

namespace ariel::ipc
{
    cv::Mat State::calcTransform(cv::Size size)
    {
        std::vector<cv::Point2f> corners;
        cv::Mat M;
        warpMatrix(size, rotation.x, rotation.y, rotation.z, 1.0, 50,M,&corners);//Compute warp matrix

        cv::Mat M_scale(3, 3, CV_64F, 0.0);
        M_scale.at<double>(0, 0) = scale.x;
        M_scale.at<double>(1, 1) = scale.y;
        M_scale.at<double>(2, 2) = 1;
        
        transform = M * M_scale;

        return transform;
    }
    cv::Size State::calcSizeAfterTransform(cv::Size size)
    {
        double fovy = 50;
        double scale = 1.0;
        double halfFovy=fovy*0.5;
        double d=hypot(size.width, size.height);
        double sideLength=scale*d/cos(deg2Rad(halfFovy));

        return cv::Size(sideLength,sideLength);
    }

    ImageProcessor::ImageProcessor()
    {
        m_state.mode = Mode::CALIBRATION;
        m_pipeline = std::make_unique<MotorCalibrationPipeline>(*this, 30);
    }

    ImageProcessor::ImageProcessor(const State& oldState)
        : m_state(oldState)
    {
        m_state.mode = Mode::DEMO;
        m_pipeline = std::make_unique<MotorCalibrationPipeline>(*this, 30);
    }

    void ImageProcessor::processFrame(cv::Mat rawFrame)
    {
        cv::Mat frame;
        std::vector<cv::Point2f> corners;

        int key = cv::waitKey(1);

        switch(m_state.mode)
        {
            case Mode::CALIBRATION:
                m_pipeline->processFrame(rawFrame);
                if constexpr (DEBUG_EN)
                    if (! m_pipeline->getDebugFrame().empty())
                        cv::imshow("Debug", m_pipeline->getDebugFrame());

                if (m_pipeline->isFinished())
                {
                    m_pipeline.reset();
                    m_state.mode = Mode::POSE_SETUP;
                }
                frame = rawFrame;
                break;
            case Mode::POSE_SETUP:
            {
                cv::Mat M = m_state.calcTransform(rawFrame.size());
                cv::Mat tmp;
                cv::Size newSize = m_state.calcSizeAfterTransform(rawFrame.size());
                tmp = cv::Mat::zeros(newSize, rawFrame.type());
                cv::warpPerspective(rawFrame, tmp, M, tmp.size());

                std::vector<cv::Point2f> srcPoints, dstPoints;
                srcPoints.push_back(m_state.center);
                cv::perspectiveTransform(srcPoints, dstPoints, M);
                
                cv::circle( tmp, dstPoints[0], 1, cv::Scalar(0,100,100), 3, cv::LINE_AA);
                cv::circle( tmp, dstPoints[0], m_state.outerRadius, cv::Scalar(255,0,255), 3, cv::LINE_AA);
                cv::circle( tmp, dstPoints[0], m_state.innerRadius, cv::Scalar(255,255,0), 3, cv::LINE_AA);

                cv::Rect2d roi = m_state.roi(dstPoints[0]);
                if (roi.x + roi.width > tmp.cols)
                    roi.width -= roi.x + roi.width - tmp.cols;

                if (roi.y + roi.height > tmp.rows)
                    roi.height -= roi.y + roi.height - tmp.rows;
                    
                frame = tmp(roi);
                
                if (key == 'y')
                    m_state.scale.x += 0.01;
                else if (key == 'h')
                    m_state.scale.x -= 0.01;
                else if (key == 'u')
                    m_state.scale.y += 0.01;
                else if (key == 'j')
                    m_state.scale.y -= 0.01;
                else if (key == 'r')
                    m_state.center.x += 1;
                else if (key == 'f')
                    m_state.center.x -= 1;
                else if (key == 't')
                    m_state.center.y += 1;
                else if (key == 'g')
                    m_state.center.y -= 1;
                else if (key == 'q')
                    m_state.rotation.x += 1;
                else if (key == 'a')
                    m_state.rotation.x -= 1;
                else if (key == 'w')
                    m_state.rotation.y += 1;
                else if (key == 's')
                    m_state.rotation.y -= 1;
                else if (key == 'e')
                    m_state.rotation.z += 1;
                else if (key == 'd')
                    m_state.rotation.z -= 1;
                else if (key == ' ')
                {
                    m_state.mode = Mode::DEMO;
                    m_state.center = dstPoints[0];
                }
                break;
            }
            case Mode::DEMO:
            {
                cv::Size newSize = m_state.calcSizeAfterTransform(rawFrame.size());
                frame = cv::Mat::zeros(newSize, rawFrame.type());
                cv::warpPerspective(rawFrame, frame, m_state.transform, frame.size());
                
                // cv::circle( frame, m_state.center, 1, cv::Scalar(0,100,100), 3, cv::LINE_AA);
                // cv::circle( frame, m_state.center, m_state.outerRadius, cv::Scalar(255,0,255), 3, cv::LINE_AA);
                // cv::circle( frame, m_state.center, m_state.innerRadius, cv::Scalar(255,255,0), 3, cv::LINE_AA);

                frame = frame(m_state.roi());
                if (m_pipeline)
                    m_pipeline->processFrame(frame);

                if constexpr (DEBUG_EN)
                    if (m_pipeline && ! m_pipeline->getDebugFrame().empty())
                        cv::imshow("Debug", m_pipeline->getDebugFrame());

                break;
            }
        }

        cv::imshow("Frame", frame);
    }

    void ImageProcessor::setMotorMoveInfo(const MotorMoveInfo& moveInfo)
    {
        if (moveInfo.direction == MotorDirection::STILL)
        {
            spdlog::debug("Setting motor still pipeline");
            m_pipeline = std::make_unique<MotorStillPipeline>(*this);
        }
        else if (moveInfo.direction == MotorDirection::CLOCKWISE || moveInfo.direction == MotorDirection::ANTICLOCKWISE)
        {
            spdlog::debug("Setting motor moving pipeline");
            m_pipeline = std::make_unique<MotorMovingPipeline>(*this, moveInfo);
        }
        else
        {
            spdlog::warn("Unknown motor move");
        }
        
    }
}