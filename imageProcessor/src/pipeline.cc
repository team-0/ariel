#include "ipc/pipeline.hpp"
#include "ipc/imageProcessor.hpp"

#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <spdlog/spdlog.h>

namespace
{
    constexpr float GAMMA_FACTOR = 3.19;
    constexpr bool DEBUG_EN = true;

    void GammaCorrection(cv::Mat& src, cv::Mat& dst, float fGamma)
    {
        using namespace cv;
        unsigned char lut[256];

        for (int i = 0; i < 256; i++)
        {
            lut[i] = saturate_cast<uchar>(pow((float)(i / 255.0), fGamma) * 255.0f);
        }

        dst = src.clone();

        const int channels = dst.channels();

        switch (channels)
        {
            case 1: {
                MatIterator_<uchar> it, end;

                for (it = dst.begin<uchar>(), end = dst.end<uchar>(); it != end; it++)
                    *it = lut[(*it)];

                break;
            }

            case 3: {
                MatIterator_<Vec3b> it, end;

                for (it = dst.begin<Vec3b>(), end = dst.end<Vec3b>(); it != end; it++)
                {
                    (*it)[0] = lut[((*it)[0])];
                    (*it)[1] = lut[((*it)[1])];
                    (*it)[2] = lut[((*it)[2])];
                }

                break;
            }
        }
    }

    int nthPercentile(int percentile, cv::Mat picture, std::vector<uint32_t> buffer)
    {
        double max, min;
        cv::minMaxLoc(picture, &min, &max);

        const std::uint32_t divider = (std::uint32_t)((picture.rows * picture.cols) * (percentile / 100.0));
        buffer.resize(0);
        buffer.resize((int)max, 0);

        for (std::size_t i = 0; i < picture.rows * picture.cols; i++)
        {
            std::uint32_t val = 0;
            switch (picture.type())
            {
                case CV_8U:
                    val = picture.data[i];
                    break;
                case CV_32F:
                    val = (uint32_t)(((float*)picture.data)[i]);
                    break;
                case CV_64F:
                    val = (uint32_t)(((double*)picture.data)[i]);
                    break;
                default:
                    throw std::runtime_error(fmt::format("Unknown picture type: {}", picture.type()));
                    break;
            }
            if (val <= buffer.size() - 1)
                buffer[val]++;
            else
                buffer[buffer.size() - 1]++;
        }

        std::uint32_t count = 0;
        for (std::size_t i = 0; i < buffer.size(); i++)
        {
            count += buffer[i];
            if (count > divider)
                return i;
        }

        assert(false);
        return buffer.size();
    }

}

namespace ariel::ipc
{
    MotorCalibrationPipeline::MotorCalibrationPipeline(ImageProcessor& processor, size_t framesCount)
        : Pipeline(processor)
        , m_totalFramesCount(framesCount)
        , m_framesCount(0)
    {
        m_circles.reserve(framesCount);
    }

    void MotorCalibrationPipeline::processFrame(cv::Mat frame)
    {
        if (isFinished()) return;

        // Image correction
        cvtColor(frame, m_grayImg, cv::COLOR_BGR2GRAY);
        cv::medianBlur(m_grayImg, m_grayImg, 11);
        GammaCorrection(m_grayImg, m_grayImg, GAMMA_FACTOR);

        // Image processing
        std::vector<cv::Vec3f> circles;

        cv::HoughCircles(m_grayImg, circles, cv::HOUGH_GRADIENT, 1,
                    m_grayImg.rows/16,  // change this value to detect circles with different distances to each other
                    200, 50, 200, 0 // change the last two parameters
                // (min_radius & max_radius) to detect larger circles
        );

        // Debug output
        if constexpr(DEBUG_EN)
        {
            for(cv::Vec3f& circle : circles)
            {
                cv::Point2f center = cv::Point2f(circle[0], circle[1]);
                float radius = circle[2];
                // circle center
                cv::circle( m_grayImg, center, 1, cv::Scalar(255), 3, cv::LINE_AA);
                // circle outline
                cv::circle( m_grayImg, center, radius, cv::Scalar(255), 3, cv::LINE_AA);
            }
        }

        if (circles.size() == 0)
            spdlog::warn("[MotorCalibrationPipeline] No circles found in frame, ignoring");
        else if (circles.size() > 2)
            spdlog::warn("[MotorCalibrationPipeline] Found {} circles in frame, ignoring", circles.size());
        else
        {
            for(auto& circle : circles)
                m_circles.push_back({cv::Point2f(circle[0], circle[1]), circle[2]});
            m_framesCount++;
        }

        if (isFinished())
        {
            processCollectedData(frame);
        }
    }

    void MotorCalibrationPipeline::processCollectedData(cv::Mat lastFrame)
    {
        cv::Point2d theTrueCenter(0.0, 0.0);

        for(auto& c : m_circles)
        {
            theTrueCenter.x += c.center.x;
            theTrueCenter.y += c.center.y;
        }

        theTrueCenter.x /= m_circles.size();
        theTrueCenter.y /= m_circles.size();

        std::vector<Circle> acceptedCircles;
        acceptedCircles.reserve(m_circles.size());

        double sd = 0.0;
        for(auto& c : m_circles)
        {
            double distance = cv::norm(cv::Point2d(c.center) - theTrueCenter);
            sd += distance * distance;
        }

        sd = sqrt(sd);
        sd *= 3;

        for(auto& c : m_circles)
        {
            double distance = cv::norm(cv::Point2d(c.center) - theTrueCenter);
            if (distance < sd)
                acceptedCircles.push_back(c);
        }

        cv::Point2d theOneAndOnlyBestCircleEver(0.0, 0.0);
        for(auto& c : acceptedCircles)
        {
            theOneAndOnlyBestCircleEver.x += c.center.x;
            theOneAndOnlyBestCircleEver.y += c.center.y;
        }

        theOneAndOnlyBestCircleEver.x /= acceptedCircles.size();
        theOneAndOnlyBestCircleEver.y /= acceptedCircles.size();
        
        double meanRadius = 0.0;
        for (auto& c : acceptedCircles)
            meanRadius += c.radius;
        
        meanRadius /= acceptedCircles.size();

        std::vector<Circle> suspectedOutterCircles;
        suspectedOutterCircles.reserve(acceptedCircles.size());
        for (auto& c : acceptedCircles)
            if (c.radius > meanRadius)
                suspectedOutterCircles.push_back(c);
        

        sd = 0.0;
        for (auto& c : suspectedOutterCircles)
            sd += pow(meanRadius - c.radius, 2);
        
        sd = sqrt(sd);
        sd *= 3;

        double theOneAndOnlyBestOutterRadius = 0.0;
        int count = 0;
        for (auto& c : suspectedOutterCircles)
            if ( abs(meanRadius - c.radius) < sd )
            {
                theOneAndOnlyBestOutterRadius += c.radius;
                count++;
            }

        theOneAndOnlyBestOutterRadius /= count;

        m_processor.state().center = theOneAndOnlyBestCircleEver;
        m_processor.state().outerRadius = theOneAndOnlyBestOutterRadius;
        m_processor.state().innerRadius = theOneAndOnlyBestOutterRadius  * 0.75;

        spdlog::debug("[MotorCalibrationPipeline] Found outer circle at X: {}, Y: {} with radius {}", theOneAndOnlyBestCircleEver.x, theOneAndOnlyBestCircleEver.y, theOneAndOnlyBestOutterRadius);
        if constexpr(DEBUG_EN)
        {
            cv::Mat img;
            lastFrame.copyTo(img);

            cv::circle( img, theOneAndOnlyBestCircleEver, 1, cv::Scalar(0,100,100), 3, cv::LINE_AA);
            cv::circle( img, theOneAndOnlyBestCircleEver, theOneAndOnlyBestOutterRadius, cv::Scalar(255,0,255), 3, cv::LINE_AA);
            cv::imshow("Calibrated circle", img);
        }
    }

    bool MotorCalibrationPipeline::isFinished() const
    {
        return m_circles.size() >= m_totalFramesCount;
    }

    void MotorStillPipeline::processFrame(cv::Mat rawFrame)
    {
        cv::cvtColor(rawFrame, m_grayImg, cv::COLOR_BGR2GRAY);
        int topVal = nthPercentile(90, m_grayImg, m_buffer);
        m_grayImg *= 255.0 / topVal;
        cv::medianBlur(m_grayImg, m_grayImg, 11);
        double lookRadius = m_processor.state().innerRadius * 0.93;

        cv::Point center = rawFrame.size() / 2;
        cv::Size axes (lookRadius, lookRadius); // Ellipse axis dimensions, divided by 2, here the diameter will be 20 pixels
        std::vector<cv::Point2i> points_on_circle;
        ellipse2Poly(center, axes, 0, 0, 360, 1, points_on_circle);
        
        int brightestValue = 0;
        size_t brightestIndex = 0;

        for (size_t i = 0; i < points_on_circle.size(); i++)
        {
            cv::Point p = points_on_circle[i];
            uint8_t val = m_grayImg.at<uint8_t>(p);
            if (val > brightestValue)
            {
                brightestValue = val;
                brightestIndex = i;
            }
        }

        size_t startBrightnessIndex = brightestIndex;
        for (size_t i = 1; i < points_on_circle.size(); i++)
        {
            size_t realIndex = (360 + (brightestIndex - i)) % points_on_circle.size();
            cv::Point p = points_on_circle[realIndex];
            uint8_t val = m_grayImg.at<uint8_t>(p);
            if (val < brightestValue - 20)
            {
                startBrightnessIndex = realIndex;
                break;
            }
        }

        size_t lastBrightestIndex = brightestIndex;

        for (size_t i = 1; i < points_on_circle.size(); i++)
        {
            size_t realIndex = (i + brightestIndex) % points_on_circle.size();
            cv::Point p = points_on_circle[realIndex];
            uint8_t val = m_grayImg.at<uint8_t>(p);
            if (val < brightestValue - 20)
            { 
                lastBrightestIndex = realIndex;
                break;
            }
        }

        double angle = (lastBrightestIndex + startBrightnessIndex) / 2.0;
        if (angle > 360.0) angle -= 360.0;
        m_processor.state().currentAngle = angle;

        if constexpr(DEBUG_EN)
        {
            spdlog::debug("Angle: {}", angle);
            cv::circle(m_grayImg, rawFrame.size()/2, lookRadius, cv::Scalar(0), 2, cv::LINE_AA);
            cv::circle(m_grayImg, points_on_circle[startBrightnessIndex], 1, cv::Scalar(0), 3, cv::LINE_AA);
            cv::circle(m_grayImg, points_on_circle[lastBrightestIndex], 1, cv::Scalar(25), 3, cv::LINE_AA);
        }
    }

    void MotorMovingPipeline::processFrame(cv::Mat rawFrame)
    {
        cv::cvtColor(rawFrame, m_grayImg, cv::COLOR_BGR2GRAY);
        int topVal = nthPercentile(90, m_grayImg, m_buffer);
        m_grayImg *= 255.0 / topVal;
        cv::medianBlur(m_grayImg, m_grayImg, 11);
        double lookRadius = m_processor.state().innerRadius * 0.93;

        cv::Point center = rawFrame.size() / 2;
        cv::Size axes (lookRadius, lookRadius); // Ellipse axis dimensions, divided by 2, here the diameter will be 20 pixels
        std::vector<cv::Point2i> points_on_circle;
        ellipse2Poly(center, axes, 0, 0, 360, 1, points_on_circle);
        
        int previousIndex = (int) m_processor.state().currentAngle;
        spdlog::debug("Previous index: {}", previousIndex);
        
        int brightestValue = 0;
        size_t brightestIndex = 0;

        for (size_t i = 0; i < points_on_circle.size() / 3; i++)
        {
            size_t realIndex;
            if (m_moveInfo.direction == MotorDirection::ANTICLOCKWISE)
                realIndex = (360 + (previousIndex - i)) % points_on_circle.size();
            else
                realIndex = (previousIndex + i) % points_on_circle.size();
            
            cv::Point p = points_on_circle[realIndex];
            uint8_t val = m_grayImg.at<uint8_t>(p);
            if (val > brightestValue)
            {
                brightestValue = val;
                brightestIndex = realIndex;
            }
        }

        size_t startBrightnessIndex = brightestIndex;
        for (size_t i = 1; i < points_on_circle.size(); i++)
        {
            size_t realIndex = (360 + (brightestIndex - i)) % points_on_circle.size();
            cv::Point p = points_on_circle[realIndex];
            uint8_t val = m_grayImg.at<uint8_t>(p);
            if (val < brightestValue - 20)
            {
                startBrightnessIndex = realIndex;
                break;
            }
        }

        size_t lastBrightestIndex = brightestIndex;

        for (size_t i = 1; i < points_on_circle.size(); i++)
        {
            size_t realIndex = (i + brightestIndex) % points_on_circle.size();
            cv::Point p = points_on_circle[realIndex];
            uint8_t val = m_grayImg.at<uint8_t>(p);
            if (val < brightestValue - 20)
            { 
                lastBrightestIndex = realIndex;
                break;
            }
        }

        double angle = (lastBrightestIndex + startBrightnessIndex) / 2.0;
        if (angle > 360.0) angle -= 360.0;
        m_processor.state().currentAngle = angle;

        if constexpr(DEBUG_EN)
        {
            spdlog::debug("Angle: {}", angle);
            cv::circle(m_grayImg, rawFrame.size()/2, lookRadius, cv::Scalar(0), 2, cv::LINE_AA);
            cv::circle(m_grayImg, points_on_circle[startBrightnessIndex], 1, cv::Scalar(0), 3, cv::LINE_AA);
            cv::circle(m_grayImg, points_on_circle[lastBrightestIndex], 1, cv::Scalar(25), 3, cv::LINE_AA);
            cv::circle(m_grayImg, points_on_circle[previousIndex], 1, cv::Scalar(70), 4, cv::LINE_AA);
            cv::circle(m_grayImg, points_on_circle[(previousIndex + points_on_circle.size()/3) % points_on_circle.size()], 1, cv::Scalar(70), 4, cv::LINE_AA);
        }
    }
}
