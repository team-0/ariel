#pragma once

#include <string>

namespace ariel {
  class Config
  {
    std::string m_rtsp_url = "";
    std::string m_mqtt_host = "";
    int m_mqtt_port = 0;

  public:
    Config();

    inline const std::string &
    rtspUrl() const
    {
      return m_rtsp_url;
    }

    inline const std::string &
    mqttHost() const
    {
      return m_mqtt_host;
    }

    inline int
    mqttPort() const
    {
      return m_mqtt_port;
    }

    template<typename StringType>
    inline void
    rtspUrl(StringType &&str)
    {
      this->m_rtsp_url = std::forward<StringType>(str);
    }

    template<typename StringType>
    inline void
    mqttHost(StringType &&str)
    {
      this->m_mqtt_host = std::forward<StringType>(str);
    }

    inline void
    mqttPort(int port)
    {
      this->m_mqtt_port = port;
    }
  };
} // namespace ariel
