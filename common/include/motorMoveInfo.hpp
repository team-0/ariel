#pragma once

#include <cstdint>

namespace ariel
{
    enum class MotorDirection
    {
        CLOCKWISE,
        ANTICLOCKWISE,
        STILL
    };

    struct MotorMoveInfo
    {
        MotorDirection direction;
        std::uint_fast8_t speed; // 0 - 50
    };
}