#ifndef __COMMAND_HPP__
#define __COMMAND_HPP__
#include <stdint.h>

namespace command
{
	enum commandType : uint8_t
	{
		rotate = 0,
		pause = 1,
		stop = 2
	};

	enum rotateDirection : uint8_t
	{
		left = 0,
		right = 1
	};

	struct command
	{
		uint8_t type;
		uint8_t rotate_direction;
		uint32_t time_or_angle;
	};
}




#endif // __COMMAND_HPP__
