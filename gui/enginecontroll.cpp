#include "enginecontroll.h"
#include "ui_enginecontroll.h"

EngineControll::EngineControll(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EngineControll)
{
    ui->setupUi(this);
}

EngineControll::~EngineControll()
{
    delete ui;
}

void EngineControll::on_StopButton_released()
{
    stopEngine();
}

void EngineControll::on_ResetButton_released()
{
    resetEngine();
}

void EngineControll::on_TurnRightButton_released()
{
    turnEngineRight(ui->speedInput->text().toUInt(), ui->angleInput->text().toUInt());
}

void EngineControll::on_turnLeftButton_released()
{
    turnEngineLeft(ui->speedInput->text().toUInt(), ui->angleInput->text().toUInt());
}
