#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "enginecontroll.h"
#include "scriptcontroll.h"
#include "enginemonitor.h"
#include <config.hpp>
#include "../parser/include/parser.hpp"
#include "../common/include/command.hpp"
#include <vector>
#include "PID_v1.h"
#include <thread>
#include "../motorDriver/include/motorDriver.hpp"
#include "ipc/imageProcessor.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(ariel::Config& config, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void parseScript(QString commands);
    void stopEngine();
    void turnEngineRight(unsigned int speed, unsigned int angle);
    void turnEngineLeft(unsigned int speed, unsigned int angle);

signals:
    void syntaxError(int line);
//    void sendSpeedMonitor(unsigned int speed);
//    void sendAngleMonitor(unsigned int angle);

private:
    ariel::Config& m_config;
    Ui::MainWindow *ui;
    EngineControll* engineControll_;
    ScriptControll* scriptControll_;
    EngineMonitor* engineMonitor_;
    MyPID pid_;
    ariel::MotorDriver md;
    ariel::ipc::ImageProcessor camera;
    bool validate();
    double pid_input;
    double pid_output;
    double pid_treshold;
    bool threadSwitch;
};

#endif // MAINWINDOW_H
