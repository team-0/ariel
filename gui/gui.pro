#-------------------------------------------------
#
# Project created by QtCreator 2019-09-14T22:17:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH +=  ../parser/include/ \
    ../common/include/ \
    ../motorDriver/include/ \
    ../pidDriver/include/ \
    ../deps/mosquitto/include/



SOURCES += \
        main.cpp \
        mainwindow.cpp \
    enginecontroll.cpp \
    scriptcontroll.cpp \
    enginemonitor.cpp \
    ../parser/src/parser.cc \
    ../pidDriver/src/PID_v1.cpp \
    ../motorDriver/src/motorDriver.cc

HEADERS += \
        mainwindow.h \
    enginecontroll.h \
    scriptcontroll.h \
    enginemonitor.h \
    ../parser/include/parser.hpp \
    ../common/include/command.hpp \
    ../motorDriver/include/motorDriver.hpp \
    ../pidDriver/include/PID_v1.h

FORMS += \
        mainwindow.ui \
    enginecontroll.ui \
    scriptcontroll.ui \
    enginemonitor.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
