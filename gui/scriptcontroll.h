#ifndef SCRIPTCONTROLL_H
#define SCRIPTCONTROLL_H

#include <QWidget>

namespace Ui {
class ScriptControll;
}

class ScriptControll : public QWidget
{
    Q_OBJECT

public:
    explicit ScriptControll(QWidget *parent = nullptr);
    ~ScriptControll();

private slots:
    void on_runScriptButton_released();
    void syntaxError(int line);

signals:
    void requestScriptParse(QString script);

private:
    Ui::ScriptControll *ui;
};

#endif // SCRIPTCONTROLL_H
