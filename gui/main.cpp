#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app( argc, argv );
    ariel::Config config;
    config.mqttHost("192.168.0.1");
    // config.mqttHost("localhost");
    config.mqttPort(1883);
    
    MainWindow wnd(config);
    wnd.show();
    return app.exec();
}
