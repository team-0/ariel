#ifndef ENGINEMONITOR_H
#define ENGINEMONITOR_H

#include <QWidget>

namespace Ui {
class EngineMonitor;
}

class EngineMonitor : public QWidget
{
    Q_OBJECT

public:
    explicit EngineMonitor(QWidget *parent = nullptr);
    ~EngineMonitor();

private slots:
    void sendSpeedMonitor(unsigned int speed);
//    void sendAngleMonitor(unsigned int angle);


private:
    Ui::EngineMonitor *ui;
};

#endif // ENGINEMONITOR_H
