#include "cmath"
struct MyPID
{
    double ki, kd, kp;
    double setpoint;
    double integral = 0.0;
    double last_error = 0.0;

    double calculate(double value)
    {
        double error = setpoint - value;
        while (error > M_PI) error -= M_2_PI;
        while (error < -M_PI) error += M_2_PI;

        integral += error;

        double P = kp * error;
        double I = ki * integral;
        double D = (error - last_error) * kd;

        last_error = error;

        double PID = P + I + D;
        if (PID > 1) PID = 1.0;
        else if (PID < -1) PID = -1.0;

        return PID;
    }
};
