#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <unistd.h>

MainWindow::MainWindow(ariel::Config& config, QWidget *parent) :
    QMainWindow(parent),
    m_config(config),
    ui(new Ui::MainWindow),
    md(config)
{
    ui->setupUi(this);
    threadSwitch = false;
    engineControll_ = new EngineControll(this);
    scriptControll_ = new ScriptControll(this);
    engineMonitor_ = new EngineMonitor(this);
    ui->verticalLayout->insertWidget(0, engineMonitor_);
    ui->horizontalLayout->addWidget(scriptControll_);
    ui->horizontalLayout->addWidget(engineControll_);
    connect(scriptControll_, SIGNAL(requestScriptParse(QString)), this, SLOT(parseScript(QString)));
    connect(this, SIGNAL(syntaxError(int)), scriptControll_, SLOT(syntaxError(int)));
    connect(engineControll_, SIGNAL(stopEngine()), this, SLOT(stopEngine()));
    connect(engineControll_, SIGNAL(turnEngineLeft(unsigned int, unsigned int)), this, SLOT(turnEngineLeft(unsigned int, unsigned int)));
    connect(engineControll_, SIGNAL(turnEngineRight(unsigned int, unsigned int)), this, SLOT(turnEngineRight(unsigned int, unsigned int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::parseScript(QString commands)
{
    std::vector<command::command> commandsVector;
    if(parse_string(commands.toStdString(), commandsVector))
    {
        //do action
    }
    else
    {
        syntaxError(commandsVector.size());
    }
}

void MainWindow::stopEngine()
{
    threadSwitch = false;
    md.move({ariel::MotorDirection::STILL, 0});
    //STOP ENGINE
}

void MainWindow::turnEngineLeft(unsigned int speed, unsigned int angle)
{
    std::cout << "Thread starting" << std::endl;
    md.move({ariel::MotorDirection::ANTICLOCKWISE, static_cast<uint_fast8_t>(speed)});
}

void MainWindow::turnEngineRight(unsigned int speed, unsigned int angle)
{
    md.move({ariel::MotorDirection::CLOCKWISE, static_cast<uint_fast8_t>(speed)});
}

bool MainWindow::validate()
{
    return (this->pid_input < pid_.setpoint);
}
