#ifndef ENGINECONTROLL_H
#define ENGINECONTROLL_H

#include <QWidget>

namespace Ui {
class EngineControll;
}

class EngineControll : public QWidget
{
    Q_OBJECT

public:
    explicit EngineControll(QWidget *parent = nullptr);
    ~EngineControll();

private slots:
    void on_StopButton_released();

    void on_ResetButton_released();

    void on_TurnRightButton_released();

    void on_turnLeftButton_released();

signals:
    void stopEngine();
    void resetEngine();
    void turnEngineRight(unsigned int speed, unsigned int angle);
    void turnEngineLeft(unsigned int speed, unsigned int angle);


private:
    Ui::EngineControll *ui;
};

#endif // ENGINECONTROLL_H
