#include "scriptcontroll.h"
#include "ui_scriptcontroll.h"

ScriptControll::ScriptControll(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScriptControll)
{
    ui->setupUi(this);
}

ScriptControll::~ScriptControll()
{
    delete ui;
}

void ScriptControll::on_runScriptButton_released()
{
    requestScriptParse(ui->scriptInput->toPlainText());
}

void ScriptControll::syntaxError(int line)
{
    ui->scriptInput->clear();
}
