#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SRC_DIR=$DIR/opencv-src
BUILD_DIR=$DIR/opencv-build
INSTALL_DIR=$DIR/opencv
CONTRIB_DIR=$DIR/opencv-contrib

GIT_REPO=https://github.com/opencv/opencv
GIT_TAG=3.4.6
CONTRIB_REPO=https://github.com/opencv/opencv_contrib
CONTRIB_TAG=3.4.6

rm -rf $SRC_DIR $BUILD_DIR $INSTALL_DIR

mkdir -p $SRC_DIR
git clone --branch $GIT_TAG --depth 1 $GIT_REPO $SRC_DIR

mkdir -p $CONTRIB_DIR
git clone --branch $CONTRIB_TAG --depth 1 $CONTRIB_REPO $CONTRIB_DIR

echo "cloned"
mkdir -p $BUILD_DIR
mkdir -p $INSTALL_DIR
cd $BUILD_DIR
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DENABLE_CXX11=ON -DWITH_OPENGL=ON -DWITH_OPENNI2=ON -DWITHVTK=ON -DOPENCV_EXTRA_MODULES_PATH=$CONTRIB_DIR/modules -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR $SRC_DIR
make -j16
make install

#rm -rf $SRC_DIR $BUILD_DIR
