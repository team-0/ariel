#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SRC_DIR=$DIR/mosquitto-src
BUILD_DIR=$DIR/mosquitto-build
INSTALL_DIR=$DIR/mosquitto

GIT_REPO=https://github.com/eclipse/mosquitto

rm -rf $SRC_DIR $BUILD_DIR $INSTALL_DIR

mkdir -p $SRC_DIR
git clone --depth 1 $GIT_REPO $SRC_DIR

mkdir -p $BUILD_DIR
mkdir -p $INSTALL_DIR
cd $BUILD_DIR
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR $SRC_DIR

make -j8

# patch installing manuals, its causing errors 
> $BUILD_DIR/man/cmake_install.cmake

make install

#rm -rf $SRC_DIR $BUILD_DIR
